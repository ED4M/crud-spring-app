package com.netcracker.springdemo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "people")
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    private String person_name;

    public String getName() {
        return this.person_name;
    }

    public void setName(String person_name) {
        this.person_name = person_name;
    }
}
