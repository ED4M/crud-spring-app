package com.netcracker.springdemo;

public class PersonNotFoundException extends RuntimeException {
    public PersonNotFoundException (Long person_id) {
        super(String.format("Book is not found with id : '%s'", person_id));
    }
}
