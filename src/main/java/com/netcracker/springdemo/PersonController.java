package com.netcracker.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class PersonController {

    @Autowired
    PersonRepository personRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public ModelAndView welcome() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("welcome.html");
        return modelAndView;
    }

    @GetMapping("/people")
    public List getAllNotes() {
        return personRepository.findAll();
    }

    @PostMapping("/people")
    public Person createNote(@RequestBody Person person) {
        return personRepository.save(person);
    }

    @GetMapping("/people/{id}")
    public Person getPersonById(@PathVariable(value = "id") Long personId) throws PersonNotFoundException {
        return personRepository.findById(personId)
                .orElseThrow(() -> new PersonNotFoundException(personId));
    }

    @PutMapping("/people/{id}")
    public Person updateName(@PathVariable(value = "id") Long personId,
                           @RequestBody Person personDetails) throws PersonNotFoundException {

        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new PersonNotFoundException(personId));

        person.setName(personDetails.getName());

        Person updatedPerson = personRepository.save(person);
        return updatedPerson;
    }

    @DeleteMapping("/people/{id}")
    public ResponseEntity deletePerson  (@PathVariable(value = "id") Long personId) throws PersonNotFoundException {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new PersonNotFoundException(personId));

        personRepository.delete(person);
        return ResponseEntity.ok().build();
    }
}
