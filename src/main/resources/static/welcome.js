document.addEventListener('DOMContentLoaded', function(){
    let inputField = document.querySelector('.name-input');
    let displayNameElement = document.querySelector('.name-content');

    renderName(displayNameElement);

    document.querySelector('.update-btn').addEventListener('click', () => {
        let data = {id: 0, name:inputField.value};
        fetch("/people/1", {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(res => {
                if (res.status < 300) {
                    return res.json();
                } else {
                    displayNameElement.innerHTML = "<h1 style='color: #ff0000'>Error renaming person!</h1>";
                }
            })
            .then(result => {
                displayNameElement.textContent = `Hello ${result.name}!`;
            })
    })
});

function renderName(displayNameElement) {
    fetch("/people/1")
        .then(res => res.json())
        .then(res => {
            console.log(res);
            displayNameElement.textContent = `Hello ${res.name}!`;
        });
}